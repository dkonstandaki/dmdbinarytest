package com.dk.dmdbinarytest.ui.fragment;

import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.dk.dmdbinarytest.R;
import com.dk.dmdbinarytest.TestApp;
import com.dk.dmdbinarytest.api.TestApi;
import com.dk.dmdbinarytest.model.Item;
import com.dk.dmdbinarytest.model.ItemDao;

import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ItemsFragment extends ListFragment {

    private ItemDao mItemDao;
    private Item[] mItems = new Item[]{};

    private Unbinder mUnbinder;

    @BindView(R.id.clItemsContainer)
    CoordinatorLayout clContainer;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_items, container, false);
        mUnbinder = ButterKnife.bind(this, v);
        return v;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mItemDao = ((TestApp) getActivity().getApplication()).getDaoSession().getItemDao();
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(R.string.screen_list);
        loadItemsFromDatabase();
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        Bundle bundle = new Bundle();
        bundle.putParcelable(ItemDetailsFragment.KEY_ITEM, mItems[position]);
        Fragment fragment = new ItemDetailsFragment();
        fragment.setArguments(bundle);
        getFragmentManager().beginTransaction()
                .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out,
                        android.R.anim.fade_in, android.R.anim.fade_out)
                .replace(R.id.flContainer, fragment)
                .addToBackStack(null)
                .commit();
    }

    private void loadItemsFromDatabase() {
        List<Item> itemsList = mItemDao.loadAll();
        mItems = itemsList.toArray(new Item[itemsList.size()]);
        if (mItems.length > 0)
            setListAdapter(new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, mItems));
        else
            loadItemsFromNetwork();
    }

    private void loadItemsFromNetwork() {
        progressBar.setVisibility(View.VISIBLE);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(TestApi.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        TestApi api = retrofit.create(TestApi.class);
        Call<Item[]> call = api.getItems();
        call.enqueue(new Callback<Item[]>() {
            @Override
            public void onResponse(Call<Item[]> call, Response<Item[]> response) {
                progressBar.setVisibility(View.GONE);
                if (response.body() != null && response.body().length > 0) {
                    mItems = response.body();
                    Arrays.sort(mItems);
                    mItemDao.insertInTx(mItems);
                    setListAdapter(new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, mItems));
                }
            }

            @Override
            public void onFailure(Call<Item[]> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                Snackbar.make(clContainer, R.string.server_error_msg, Snackbar.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mUnbinder.unbind();
    }
}
