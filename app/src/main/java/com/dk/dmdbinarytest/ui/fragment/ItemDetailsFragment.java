package com.dk.dmdbinarytest.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.dk.dmdbinarytest.R;
import com.dk.dmdbinarytest.TestApp;
import com.dk.dmdbinarytest.model.Item;
import com.dk.dmdbinarytest.model.ItemDao;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class ItemDetailsFragment extends Fragment {

    public static final String KEY_ITEM = "item";

    private ItemDao mItemDao;
    private Item mItem;

    private Unbinder mUnbinder;

    @BindView(R.id.clItemDetailsContainer)
    CoordinatorLayout clContainer;
    @BindView(R.id.etItemTitle)
    EditText etTitle;
    @BindView(R.id.etItemBody)
    EditText etBody;
    @BindView(R.id.tvItemCreated)
    TextView tvCreated;
    @BindView(R.id.fabSave)
    FloatingActionButton fabSave;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_item_details, container, false);
        mUnbinder = ButterKnife.bind(this, v);
        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mItemDao = ((TestApp) getActivity().getApplication()).getDaoSession().getItemDao();
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(R.string.screen_details);
        setupItemData(getArguments());
    }

    private void setupItemData(Bundle args) {
        if (args != null && args.containsKey(KEY_ITEM)) {
            mItem = args.getParcelable(KEY_ITEM);
            if (mItem != null) {
                etTitle.setText(mItem.getTitle());
                etBody.setText(mItem.getBody());
                tvCreated.setText(mItem.getCreated());
            }
        }
    }

    @OnClick(R.id.fabSave)
    public void saveItem() {
        if (performValidation()) {
            mItem.setTitle(etTitle.getText().toString());
            mItem.setBody(etBody.getText().toString());
            mItemDao.update(mItem);
            hideKeyboard();
            getActivity().onBackPressed();
        }
    }

    private boolean performValidation() {
        if (TextUtils.isEmpty(etTitle.getText()) || TextUtils.isEmpty(etBody.getText())) {
            Snackbar.make(clContainer, R.string.not_valid_msg, Snackbar.LENGTH_SHORT).show();
            return false;
        } else
            return true;
    }

    private void hideKeyboard() {
        View view = getActivity().getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mUnbinder.unbind();
    }
}
