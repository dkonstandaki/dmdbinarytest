package com.dk.dmdbinarytest;

import android.app.Application;

import com.dk.dmdbinarytest.model.DaoMaster;
import com.dk.dmdbinarytest.model.DaoSession;

import org.greenrobot.greendao.database.Database;

public class TestApp extends Application {

    private static final String DB_NAME = "items-db";

    private DaoSession daoSession;

    @Override
    public void onCreate() {
        super.onCreate();
        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(this, DB_NAME);
        Database db = helper.getWritableDb();
        daoSession = new DaoMaster(db).newSession();
    }

    public DaoSession getDaoSession() {
        return daoSession;
    }
}
