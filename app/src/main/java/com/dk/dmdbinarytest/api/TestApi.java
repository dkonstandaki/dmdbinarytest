package com.dk.dmdbinarytest.api;

import com.dk.dmdbinarytest.model.Item;

import retrofit2.Call;
import retrofit2.http.GET;

public interface TestApi {

    String BASE_URL = "https://dl.dropboxusercontent.com/u/23897589/";

    @GET("data.json")
    Call<Item[]> getItems();
}
