package com.dk.dmdbinarytest.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.greenrobot.greendao.annotation.Generated;

@Entity
public class Item implements Parcelable, Comparable<Item> {

    @Id
    private Long _ID;

    private int id;
    private String title;
    private String body;
    private String created;

    public Item() {
    }

    private Item(Parcel in) {
        _ID = in.readLong();
        id = in.readInt();
        title = in.readString();
        body = in.readString();
        created = in.readString();
    }

    @Generated(hash = 420349055)
    public Item(Long _ID, int id, String title, String body, String created) {
        this._ID = _ID;
        this.id = id;
        this.title = title;
        this.body = body;
        this.created = created;
    }

    public void writeToParcel(Parcel out, int flags) {
        out.writeLong(_ID);
        out.writeInt(id);
        out.writeString(title);
        out.writeString(body);
        out.writeString(created);
    }

    public static final Parcelable.Creator<Item> CREATOR = new Parcelable.Creator<Item>() {
        public Item createFromParcel(Parcel in) {
            return new Item(in);
        }
        public Item[] newArray(int size) {
            return new Item[size];
        }
    };

    public int describeContents() {
        return 0;
    }

    public Long get_ID() {
        return this._ID;
    }

    public void set_ID(Long _ID) {
        this._ID = _ID;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    private Date getCreationDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        Date date = new Date();
        try {
            date = dateFormat.parse(created);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    @Override
    public String toString() {
        return title;
    }

    @Override
    public int compareTo(@NonNull Item anotherItem) {
        return anotherItem.getCreationDate().compareTo(getCreationDate());
    }
}

